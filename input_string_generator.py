import sys
import string
import random

if __name__ == '__main__':
    file = open(sys.argv[1], 'w')
    for i in range(int(sys.argv[2])):
        file.write(str(i + 1) + '\t' + ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(int(sys.argv[3]))) + '\n')