import sys
import json

if __name__ == '__main__':
    file = open(sys.argv[1], 'w')
    n = 0
    for i in range(8500):
        for j in range(200):
            for k in range(36):
                if n == int(sys.argv[2]):
                    exit()
                n += 1
                file.write(str(n) + '\t' + json.dumps({
                    'month': k,
                    'city': j,
                    'product': i
                }) + '\n')


